package com.gis.web;

import com.gis.web.rest.dto.MarkerDto;
import com.gis.web.rest.request.MarkerRequest;
import com.gis.web.rest.request.MarkerSaveRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RequestMapping(value = "/gis", produces = MediaType.APPLICATION_JSON_VALUE)
public interface MarkerController {

    @GetMapping
    ResponseEntity<List<MarkerDto>> getMarkers();

    @PutMapping
    ResponseEntity<Void> save(@Valid @RequestBody MarkerSaveRequest saveRequest, BindingResult bindingResult) ;

}
