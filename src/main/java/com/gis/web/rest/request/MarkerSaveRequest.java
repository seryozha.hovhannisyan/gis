package com.gis.web.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

public class MarkerSaveRequest implements Serializable {

    private static final long serialVersionUID = 8727730186091052658L;
    @JsonProperty
    private List<MarkerRequest> markers;

    @JsonProperty
    private List<Integer> removeIds;


    public MarkerSaveRequest() {
    }

    public MarkerSaveRequest(List<MarkerRequest> markers, List<Integer> removeIds) {
        this.markers = markers;
        this.removeIds = removeIds;
    }

    public List<MarkerRequest> getMarkers() {
        return markers;
    }

    public void setMarkers(List<MarkerRequest> markers) {
        this.markers = markers;
    }

    public List<Integer> getRemoveIds() {
        return removeIds;
    }

    public void setRemoveIds(List<Integer> removeIds) {
        this.removeIds = removeIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarkerSaveRequest that = (MarkerSaveRequest) o;

        if (markers != null ? !markers.equals(that.markers) : that.markers != null) return false;
        return removeIds != null ? removeIds.equals(that.removeIds) : that.removeIds == null;
    }

    @Override
    public int hashCode() {
        int result = markers != null ? markers.hashCode() : 0;
        result = 31 * result + (removeIds != null ? removeIds.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MarkerSaveRequest.class.getSimpleName() + "[", "]")
                .add("markers=" + markers)
                .add("removeIds=" + removeIds)
                .toString();
    }
}
