package com.gis.web.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.util.StringJoiner;

public class MarkerRequest {

    @JsonProperty
    private int id;

    @JsonProperty
    @NotBlank
    private  String latitude;

    @JsonProperty
    @NotBlank
    private  String longitude;

    @JsonProperty
    private  String label;

    @JsonProperty
    private  boolean draggable;

    @JsonProperty
    private  String icon;

    @JsonProperty
    private  String type;

    public MarkerRequest(int id, @NotBlank String latitude, @NotBlank String longitude, String label, boolean draggable, String icon, String type) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.label = label;
        this.draggable = draggable;
        this.icon = icon;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDraggable() {
        return draggable;
    }

    public void setDraggable(boolean draggable) {
        this.draggable = draggable;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarkerRequest that = (MarkerRequest) o;

        if (id != that.id) return false;
        if (draggable != that.draggable) return false;
        if (!latitude.equals(that.latitude)) return false;
        if (!longitude.equals(that.longitude)) return false;
        if (label != null ? !label.equals(that.label) : that.label != null) return false;
        if (icon != null ? !icon.equals(that.icon) : that.icon != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + latitude.hashCode();
        result = 31 * result + longitude.hashCode();
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (draggable ? 1 : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MarkerRequest.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("latitude='" + latitude + "'")
                .add("longitude='" + longitude + "'")
                .add("label='" + label + "'")
                .add("draggable=" + draggable)
                .add("icon='" + icon + "'")
                .add("type='" + type + "'")
                .toString();
    }
}
