package com.gis.web.rest.impl;

import com.gis.service.MarkerService;
import com.gis.web.MarkerController;
import com.gis.web.rest.dto.MarkerDto;
import com.gis.web.rest.request.MarkerRequest;
import com.gis.web.rest.request.MarkerSaveRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:4200"})
@RestController
public class MarkerControllerImpl implements MarkerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarkerControllerImpl.class);
    private final MarkerService service;

    @Autowired
    public MarkerControllerImpl(MarkerService service) {
        this.service = service;
    }


    public ResponseEntity<List<MarkerDto>> getMarkers() {
        List<MarkerDto> dtos = service.getMarkers();
        return ResponseEntity.ok().body(dtos);
    }


    public ResponseEntity<Void> save(@Valid @RequestBody MarkerSaveRequest saveRequest,
                                     BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            LOGGER.debug("Invalid request");
            return ResponseEntity.badRequest().build();
        }

        service.save(saveRequest);
        return ResponseEntity.noContent().build();
    }
}
