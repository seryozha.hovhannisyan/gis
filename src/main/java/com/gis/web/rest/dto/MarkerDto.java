package com.gis.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

public class MarkerDto {

    @JsonProperty
    private int id;

    @JsonProperty
    private  String latitude;

    @JsonProperty
    private  String longitude;

    @JsonProperty
    private  String label;

    @JsonProperty
    private  boolean draggable;

    @JsonProperty
    private  String icon;

    @JsonProperty
    private  String type;

    public MarkerDto() {
    }

    public MarkerDto(int id, String latitude, String longitude, String label, boolean draggable, String icon, String type) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.label = label;
        this.draggable = draggable;
        this.icon = icon;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLabel() {
        return label;
    }

    public boolean isDraggable() {
        return draggable;
    }

    public String getIcon() {
        return icon;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarkerDto markerDto = (MarkerDto) o;

        if (id != markerDto.id) return false;
        if (draggable != markerDto.draggable) return false;
        if (!latitude.equals(markerDto.latitude)) return false;
        if (!longitude.equals(markerDto.longitude)) return false;
        if (label != null ? !label.equals(markerDto.label) : markerDto.label != null) return false;
        if (icon != null ? !icon.equals(markerDto.icon) : markerDto.icon != null) return false;
        return type != null ? type.equals(markerDto.type) : markerDto.type == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + latitude.hashCode();
        result = 31 * result + longitude.hashCode();
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (draggable ? 1 : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", MarkerDto.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("latitude='" + latitude + "'")
                .add("longitude='" + longitude + "'")
                .add("label='" + label + "'")
                .add("draggable=" + draggable)
                .add("icon='" + icon + "'")
                .add("type='" + type + "'")
                .toString();
    }
}