package com.gis.web.rest.exception;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 4372672070020226566L;

    @NotNull
    private ErrorCode errorCode;

    private String errorMessage;

    public ErrorResponse(@NotNull ErrorCode errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
