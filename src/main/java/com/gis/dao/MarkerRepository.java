package com.gis.dao;

import com.gis.domain.Marker;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkerRepository extends PagingAndSortingRepository<Marker, Integer> {
}
