package com.gis.service;

import com.gis.exception.ServiceException;
import com.gis.web.rest.exception.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


@ControllerAdvice
public class ExceptionHandlerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerService.class);

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public ResponseEntity<ErrorResponse> handleServiceException(
            ServiceException e) {
        LOGGER.error("Error : " + e.getMessage());
        return ResponseEntity.status(e.getErrorCode().getHttpStatus().value())
                .body(new ErrorResponse(e.getErrorCode(), e.getMessage()));
    }
}
