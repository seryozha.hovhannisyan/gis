package com.gis.service;

import com.gis.dao.MarkerRepository;
import com.gis.domain.Marker;
import com.gis.exception.ServiceException;
import com.gis.web.rest.dto.MarkerDto;
import com.gis.web.rest.exception.ErrorCode;
import com.gis.web.rest.request.MarkerRequest;
import com.gis.web.rest.request.MarkerSaveRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
@Transactional(readOnly = true)
public class MarkerService {

    MarkerRepository repository;

    @Autowired
    public MarkerService(MarkerRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void save(MarkerSaveRequest saveRequest) {
        List<Integer> removeIds = saveRequest.getRemoveIds();
        removeIds.stream().forEach(r -> {
            if (!isMarkerExist(r)) {
                throw new ServiceException(ErrorCode.NOT_FOUND, String.format("Marker doesn't exist id = [%d]", r));
            }
            repository.deleteById(r);
        });

        List<MarkerRequest> requests = saveRequest.getMarkers();
        requests.stream().forEach(r -> repository.save(map(r)));
    }


    public List<MarkerDto> getMarkers() {
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                .map(this::map).collect(Collectors.toList());
    }

    public boolean isMarkerExist (int id) {
        return repository.existsById(id);
    }

    private MarkerDto map(Marker data) {
        return new MarkerDto(data.getId(),
                data.getLatitude(),
                data.getLongitude(),
                data.getLabel(),
                data.isDraggable(), data.getIcon(),
                data.getType());
    }

    private Marker map(MarkerRequest request) {
        return new Marker(request.getId(),
                request.getLatitude(),
                request.getLongitude(),
                request.getLabel(),
                request.isDraggable(), request.getIcon(),
                request.getType());
    }

}
