package com.gis.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.StringJoiner;

@Entity
public class Marker implements Serializable {

    private static final long serialVersionUID = 1970722499693978736L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String latitude;

    @Column(nullable = false)
    private String longitude;

    @Column
    private String label;

    @Column
    private boolean draggable;

    @Column
    private String icon;

    @Column
    private String type;

    public Marker() {
    }

    public Marker(Integer id, String latitude, String longitude, String label, boolean draggable, String icon, String type) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.label = label;
        this.draggable = draggable;
        this.icon = icon;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLabel() {
        return label;
    }

    public boolean isDraggable() {
        return draggable;
    }

    public String getIcon() {
        return icon;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Marker marker = (Marker) o;

        if (draggable != marker.draggable) return false;
        if (id != null ? !id.equals(marker.id) : marker.id != null) return false;
        if (!latitude.equals(marker.latitude)) return false;
        if (!longitude.equals(marker.longitude)) return false;
        if (label != null ? !label.equals(marker.label) : marker.label != null) return false;
        if (icon != null ? !icon.equals(marker.icon) : marker.icon != null) return false;
        return type != null ? type.equals(marker.type) : marker.type == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + latitude.hashCode();
        result = 31 * result + longitude.hashCode();
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (draggable ? 1 : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Marker.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("latitude='" + latitude + "'")
                .add("longitude='" + longitude + "'")
                .add("label='" + label + "'")
                .add("draggable=" + draggable)
                .add("icon='" + icon + "'")
                .add("type='" + type + "'")
                .toString();
    }
}
