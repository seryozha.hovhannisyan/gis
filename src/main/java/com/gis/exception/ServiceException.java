package com.gis.exception;

import com.gis.web.rest.exception.ErrorCode;

public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 8303256484065597037L;

    private final ErrorCode errorCode;
    private final String message;

    public ServiceException(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
