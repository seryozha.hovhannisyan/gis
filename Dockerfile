FROM java:11
WORKDIR /
ADD target/gis-0.0.1-SNAPSHOT.jar //
EXPOSE 8080
ENTRYPOINT [ "java", "-Dspring.profiles.active=docker", "-jar", "/gis-0.0.1-SNAPSHOT.jar"]
